
# PKO transactions parser app

## Usage

### Jar

```shell
java -jar pko-transaction-parser-0.1.0.jar -i=/path/to/files -o=/path/to/output.csv
```

### Docker

#### Build

```shell
docker build --build-arg user_id=$(id -u) --build-arg group_id=$(id -g) -t pko-tx-parser:0.1.0 .
```

#### Run

```shell
docker run --rm -v /path/to/files:/data pko-tx-parser:0.1.0 -i=/data -o=/data/transactions.csv
```