package com.onrooh.txparser.integration.pko;

import com.onrooh.txparser.domain.Transaction;
import lombok.AllArgsConstructor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class TransactionConverterTest {

    private static final String ACCOUNT_NUMBER = "1234567890";
    private static final String ACCOUNT_CURRENCY = "USD";

    private static final Transaction DEFAULT_TRANSACTION = Transaction.builder()
            .account(ACCOUNT_NUMBER)
            .currency(ACCOUNT_CURRENCY)
            .operationDate(LocalDate.of(2021, 7, 16))
            .executionDate(LocalDate.of(2030, 11, 15))
            .operationId("12345678901234567")
            .operationType("Test type")
            .operationSum(-3289)
            .balance(394409)
            .description("Karta:123456******1111 Godz.20:34:28 Lokalizacja: Lodz PL Sklep Lidl")
            .build();

    @ParameterizedTest
    @MethodSource("testConvertCases")
    void testContent(ConvertTestCase testCase) {
        Transaction got = TransactionConverter.convert(ACCOUNT_NUMBER, ACCOUNT_CURRENCY, testCase.inputData);

        assertThat(got).isEqualTo(testCase.expected);
    }

    private static Stream<ConvertTestCase> testConvertCases() {
        return Stream.of(
                new ConvertTestCase("two_strings_content",
                        List.of("16.07.2021 12345678901234567 Test type -32,89 3 944,09" + System.lineSeparator(),
                                "15.11.2030 Karta:123456******1111 Godz.20:34:28 Lokalizacja: Lodz PL Sklep Lidl"
                        ),
                        DEFAULT_TRANSACTION
                ),
                new ConvertTestCase("three_strings_content",
                        List.of("16.07.2021 12345678901234567 Test type -32,89 3 944,09" + System.lineSeparator(),
                                "15.11.2030 Karta:123456******1111 Godz.20:34:28" + System.lineSeparator(),
                                "Lokalizacja: Lodz PL Sklep Lidl"
                        ),
                        DEFAULT_TRANSACTION
                ),
                new ConvertTestCase("empty_string_prefix_content",
                        List.of("",
                                "16.07.2021 12345678901234567 Test type -32,89 3 944,09" + System.lineSeparator(),
                                "15.11.2030 Karta:123456******1111 Godz.20:34:28 Lokalizacja: Lodz PL Sklep Lidl"
                        ),
                        DEFAULT_TRANSACTION
                ),
                new ConvertTestCase("empty_description",
                        List.of("16.07.2021 12345678901234567 Test type -32,89 3 944,09" + System.lineSeparator(),
                                "15.11.2030"
                        ),
                        Transaction.builder()
                                .account(ACCOUNT_NUMBER)
                                .currency(ACCOUNT_CURRENCY)
                                .operationDate(LocalDate.of(2021, 7, 16))
                                .executionDate(LocalDate.of(2030, 11, 15))
                                .operationId("12345678901234567")
                                .operationType("Test type")
                                .operationSum(-3289)
                                .balance(394409)
                                .build()
                ),
                new ConvertTestCase("type_ends_with_non_alphabetic",
                        List.of("",
                                "16.07.2021 12345678901234567 Test type. -32,89 3 944,09" + System.lineSeparator(),
                                "15.11.2030 Karta:123456******1111 Godz.20:34:28 Lokalizacja: Lodz PL Sklep Lidl"
                        ),
                        Transaction.builder()
                                .account(ACCOUNT_NUMBER)
                                .currency(ACCOUNT_CURRENCY)
                                .operationDate(LocalDate.of(2021, 7, 16))
                                .executionDate(LocalDate.of(2030, 11, 15))
                                .operationId("12345678901234567")
                                .operationType("Test type.")
                                .operationSum(-3289)
                                .balance(394409)
                                .description("Karta:123456******1111 Godz.20:34:28 Lokalizacja: Lodz PL Sklep Lidl")
                                .build()
                )
            );
    }

    @AllArgsConstructor
    static class ConvertTestCase {
        String name;
        List<String> inputData;
        Transaction expected;

        @Override
        public String toString() { return name; }
    }
}