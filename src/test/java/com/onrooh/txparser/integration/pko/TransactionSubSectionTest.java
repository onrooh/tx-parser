package com.onrooh.txparser.integration.pko;

import com.onrooh.txparser.domain.Transaction;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class TransactionSubSectionTest {

    private static final String ACCOUNT_NUMBER = "1234567890";
    private static final String ACCOUNT_CURRENCY = "EUR";

    @Test
    void testParse() {
        List<String> input = List.of(
                "17.06.2021 12345678901234567 OTWARCIE RACHUNKU 0,00 0,00",
                "17.06.2021",
                "21.06.2021 12345678901234568 WPŁATA GOTÓWKI - KOD MOBILNY 100,00 100,00",
                "21.06.2021 T:12345678901 W:A1B2D399",
                "16.07.2021 12345678901234569 ZAKUP PRZY UŻYCIU KARTY -26,71 3 976,98",
                "14.07.2021 Karta:123456******1234 Lokalizacja: Lodz PL Action A091 Nr ref:",
                "12345678901234567890123",
                "Kwota oryg.: 26,71 PLN"
        );

        List<Transaction> got = TransactionSubSection.parse(ACCOUNT_NUMBER, ACCOUNT_CURRENCY, input);

        List<Transaction> expected = List.of(
                Transaction.builder()
                        .account(ACCOUNT_NUMBER)
                        .currency(ACCOUNT_CURRENCY)
                        .operationDate(LocalDate.of(2021, 6, 17))
                        .executionDate(LocalDate.of(2021, 6, 17))
                        .operationId("12345678901234567")
                        .operationType("OTWARCIE RACHUNKU")
                        .operationSum(0)
                        .balance(0)
                        .build(),
                Transaction.builder()
                        .account(ACCOUNT_NUMBER)
                        .currency(ACCOUNT_CURRENCY)
                        .operationDate(LocalDate.of(2021, 6, 21))
                        .executionDate(LocalDate.of(2021, 6, 21))
                        .operationId("12345678901234568")
                        .operationType("WPŁATA GOTÓWKI - KOD MOBILNY")
                        .operationSum(10000)
                        .balance(10000)
                        .description("T:12345678901 W:A1B2D399")
                        .build(),
                Transaction.builder()
                        .account(ACCOUNT_NUMBER)
                        .currency(ACCOUNT_CURRENCY)
                        .operationDate(LocalDate.of(2021, 7, 16))
                        .executionDate(LocalDate.of(2021, 7, 14))
                        .operationId("12345678901234569")
                        .operationType("ZAKUP PRZY UŻYCIU KARTY")
                        .operationSum(-2671)
                        .balance(397698)
                        .description("Karta:123456******1234 Lokalizacja: Lodz PL Action A091 Nr ref: 12345678901234567890123 Kwota oryg.: 26,71 PLN")
                        .build()
        );

        assertThat(got).containsExactlyElementsOf(expected);
    }
}