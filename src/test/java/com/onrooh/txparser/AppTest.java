package com.onrooh.txparser;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Slf4j
@Disabled
class AppTest {

    private static final String INPUT_FILE = "src/test/resources/transactions.pdf";
    private static final String OUTPUT_FILE = "text-transactions.csv";

    @Test
    void appTest() {
        final App app = new App();
        app.run(INPUT_FILE, OUTPUT_FILE, App.DELIMITER, App.LINE_SEPARATOR);
    }

}
