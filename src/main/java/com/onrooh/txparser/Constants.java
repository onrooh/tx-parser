package com.onrooh.txparser;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final String VERSION = "0.1.0";

}
