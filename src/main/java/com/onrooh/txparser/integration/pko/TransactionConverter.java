package com.onrooh.txparser.integration.pko;

import com.onrooh.txparser.domain.Transaction;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

@UtilityClass
public class TransactionConverter {

    private static final BigDecimal MULTIPLICAND = BigDecimal.valueOf(100);

    private static final UnaryOperator<String> FIRST_BEFORE_SPACE = s -> s.substring(0, s.indexOf(" "));

    private static final List<Character> ADDITIONAL_SUM_CHARS = List.of(' ', ',', '-');

    public static Transaction convert(String account, String currency, List<String> lines) {
        lines = lines.stream().map(String::trim).filter(s -> !s.isBlank()).collect(Collectors.toList());

        List<String> tokens1 = tokenizeFirstLine(lines.get(0));
        List<String> tokens2 = tokenizeRestLines(lines.subList(1, lines.size()));

        return Transaction.builder()
                .account(account)
                .currency(currency)
                .operationDate(stringToDate(tokens1.get(0)))
                .operationId(tokens1.get(1))
                .operationType(tokens1.get(2))
                .operationSum(stringToAmount(tokens1.get(3)))
                .balance(stringToAmount(tokens1.get(4)))
                .executionDate(stringToDate(tokens2.get(0)))
                .description(getDescription(tokens2))
                .build();
    }

    private static List<String> tokenizeRestLines(List<String> lines) {
        String fullContent = String.join(" ", lines);

        if (fullContent.contains(" ")) {
            String executionDate = FIRST_BEFORE_SPACE.apply(fullContent);
            String description = fullContent.substring(executionDate.length() + 1);
            return List.of(executionDate, description);
        } else {
            return List.of(fullContent);
        }
    }

    private static List<String> tokenizeFirstLine(String input) {
        input = input.trim();

        String operationDate = FIRST_BEFORE_SPACE.apply(input);
        String transactionId = FIRST_BEFORE_SPACE.apply(input.substring(operationDate.length() + 1));
        String operationSum = getOperationSum(input);
        String operationType = getOperationType(input.substring(operationDate.length() + transactionId.length() + 2), operationSum);
        String balance = getBalance(input);

        return List.of(operationDate, transactionId, operationType, operationSum, balance);
    }


    private static String getOperationType(String input, String operationSum) {
        return input.substring(0, input.indexOf(operationSum) - 1);
    }

    private static String getOperationSum(String input) {
        int operationSumPosition = input.indexOf(",");
        String inputWithoutBalance = input.substring(0, operationSumPosition + 3);
        String reversed = new StringBuilder(inputWithoutBalance).reverse().toString();
        String reversedResult = reversed.chars()
                .takeWhile(TransactionConverter::isSumFieldsCharacter)
                .mapToObj(Character::toString)
                .collect(Collectors.joining());
        return new StringBuilder(reversedResult).reverse().toString().trim();
    }

    private static boolean isSumFieldsCharacter(int c) {
        return Character.isDigit(c) || ADDITIONAL_SUM_CHARS.contains((char) c);
    }

    private static String getBalance(String input) {
        int operationSumPosition = input.indexOf(",");
        return input.substring(operationSumPosition + 3).trim();
    }

    private static String getDescription(List<String> tokens2) {
        return tokens2.size() == 1 ? null : tokens2.get(1);
    }

    private static LocalDate stringToDate(String input) {
        return LocalDate.parse(input, TransactionSubSection.DATE_FORMATTER);
    }

    private static int stringToAmount(String rawAmount) {
        return new BigDecimal(rawAmount.replace(",", ".").replace(" ", "")).multiply(MULTIPLICAND).intValue();
    }

}
