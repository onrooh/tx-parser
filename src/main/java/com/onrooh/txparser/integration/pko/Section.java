package com.onrooh.txparser.integration.pko;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class Section {

    enum Type {HEADER, TRANSACTIONS, PAGE_SEPARATOR, FOOTER}

    final Type type;
    final Function<String, Optional<Type>> evalNextState;
    final List<String> content;

    public Section(Type type, Function<String, Optional<Type>> evalNextState) {
        this.type = type;
        this.evalNextState = evalNextState;
        this.content = new ArrayList<>();
    }

    public Optional<Type> handleString(String input) {
        this.content.add(input);
        return this.evalNextState.apply(input);
    }

}
