package com.onrooh.txparser.integration.pko;

import com.onrooh.txparser.domain.Transaction;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class TransactionSubSection {

    public static final String DATE_PATTERN = "dd.MM.yyyy";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);

    private enum State {HEADER, DETAILS}

    public static List<Transaction> parse(String account, String currency, List<String> input) {
        return input.stream().collect(toTransactions(account, currency));
    }

    private static ToTransactionListCollector toTransactions(String account, String currency) {
        return new ToTransactionListCollector(account, currency);
    }

    @RequiredArgsConstructor
    private static class ToTransactionListCollector implements Collector<String, List<List<String>>, List<Transaction>> {

        final String account;
        final String currency;

        State state = State.HEADER;

        @Override
        public Supplier<List<List<String>>> supplier() {
            return () -> {
                var result = new ArrayList<List<String>>();
                result.add(new ArrayList<>());
                return result;
            };
        }

        @Override
        public BiConsumer<List<List<String>>, String> accumulator() {
            return (lists, s) -> {
                List<String> txStrings = lists.get(lists.size() - 1);

                if (state == State.HEADER) {
                    state = State.DETAILS;
                } else if (isNextTransactionBegins(txStrings, s)) {
                    state = State.HEADER;
                    txStrings = new ArrayList<>();
                    lists.add(txStrings);
                }

                txStrings.add(s);
            };
        }

        private boolean isNextTransactionBegins(List<String> txStrings, String line) {
            return txStrings.size() > 1 && startsWithDate(line);
        }

        @Override
        public BinaryOperator<List<List<String>>> combiner() {
            return (lists, lists2) -> {
                lists.addAll(lists2);
                return lists;
            };
        }

        @Override
        public Function<List<List<String>>, List<Transaction>> finisher() {
            return txLinesList -> txLinesList.stream()
                    .map(list -> TransactionConverter.convert(account, currency, list))
                    .collect(Collectors.toList());
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Set.of();
        }

        private static boolean startsWithDate(String line) {
            if (line.length() < DATE_PATTERN.length()) {
                return false;
            }

            try {
                LocalDate.parse(line.substring(0, DATE_PATTERN.length()), TransactionSubSection.DATE_FORMATTER);
            } catch (DateTimeParseException e) {
                return false;
            }

            return true;
        }

    }

}
