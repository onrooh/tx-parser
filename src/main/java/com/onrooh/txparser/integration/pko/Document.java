package com.onrooh.txparser.integration.pko;

import com.onrooh.txparser.domain.Account;
import com.onrooh.txparser.domain.Transaction;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.onrooh.txparser.integration.pko.PdfTransactionSource.BANK_NAME;
import static java.util.stream.Collector.Characteristics.IDENTITY_FINISH;

public class Document {

    private static final String ACCOUNT_NUMBER_HEADER_MARKER = "Nr rachunku/karty:";
    private static final String ACCOUNT_CURRENCY_HEADER_MARKER = "Waluta rachunku:";

    @Getter
    private final List<Section> sections;

    public Document(List<String> fileLines) {
        this.sections = fileLines.stream().collect(toSectionList());
    }

    public List<Transaction> getTransactions() {
        Account account = getAccount();
        List<String> transactionsLines = getTransactionLines();

        return TransactionSubSection.parse(account.getNumber(), account.getCurrency(), transactionsLines);
    }

    private List<String> getTransactionLines() {
        return this.getSections().stream()
                .filter(s -> s.type == Section.Type.TRANSACTIONS)
                .map(s -> s.content)
                .map(l -> l.subList(0, l.size() - 1))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Account getAccount() {
        return Account.builder()
                .number(extractHeaderField(ACCOUNT_NUMBER_HEADER_MARKER))
                .currency(extractHeaderField(ACCOUNT_CURRENCY_HEADER_MARKER))
                .bank(BANK_NAME)
                .build();
    }

    private String extractHeaderField(String marker) {
        return this.getSections().stream()
                .filter(s -> s.type == Section.Type.HEADER)
                .map(s -> s.content)
                .flatMap(Collection::stream)
                .filter(s -> s.startsWith(marker))
                .map(s -> s.replace(marker, ""))
                .map(s -> s.replaceAll(" ", ""))
                .findFirst()
                .orElseThrow();
    }

    private static ToSectionListCollector toSectionList() {
        return new ToSectionListCollector();
    }

    private static class ToSectionListCollector implements Collector<String, List<Section>, List<Section>> {

        @Override
        public Supplier<List<Section>> supplier() {
            return () -> {
                ArrayList<Section> result = new ArrayList<>();
                result.add(createSection(Section.Type.HEADER));
                return result;
            };
        }

        @Override
        public BiConsumer<List<Section>, String> accumulator() {
            return (accumulator, s) -> {
                Section currentSection = accumulator.get(accumulator.size() - 1);
                currentSection.handleString(s).ifPresent(t -> accumulator.add(createSection(t)));
            };
        }

        @Override
        public BinaryOperator<List<Section>> combiner() {
            return (list1, list2) -> {
                list1.addAll(list2);
                return list1;
            };
        }

        @Override
        public Function<List<Section>, List<Section>> finisher() {
            return Function.identity();
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Set.of(IDENTITY_FINISH);
        }

        private Section createSection(Section.Type type) {
            return DocumentStructure.STRUCTURE.get(type).get();
        }
    }

}
