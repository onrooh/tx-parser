package com.onrooh.txparser.integration.pko;

import lombok.experimental.UtilityClass;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.onrooh.txparser.integration.pko.Section.Type.*;

@UtilityClass
class DocumentStructure {

    private static final Function<String, Optional<Section.Type>> headerNextSectionEval = s -> {
        if (s.startsWith("Data waluty")) {
            return Optional.of(TRANSACTIONS);
        }
        return Optional.empty();
    };

    private static final Function<String, Optional<Section.Type>> transactionsNextSectionEval = s -> {
        if (s.startsWith("Saldo do przeniesienia")) {
            return Optional.of(PAGE_SEPARATOR);
        }
        if (s.startsWith("Saldo końcowe")) {
            return Optional.of(FOOTER);
        }
        return Optional.empty();
    };

    private static final Function<String, Optional<Section.Type>> pageSeparatorNextSectionEval = s -> {
        if (s.startsWith("Data waluty")) {
            return Optional.of(TRANSACTIONS);
        }
        return Optional.empty();
    };

    private static final Function<String, Optional<Section.Type>> footerNextSectionEval = s -> Optional.empty();

    static final Map<Section.Type, Supplier<Section>> STRUCTURE = Map.of(
            HEADER, () -> new Section(HEADER, headerNextSectionEval),
            TRANSACTIONS, () -> new Section(TRANSACTIONS, transactionsNextSectionEval),
            PAGE_SEPARATOR, () -> new Section(PAGE_SEPARATOR, pageSeparatorNextSectionEval),
            FOOTER, () -> new Section(FOOTER, footerNextSectionEval)
    );

}
