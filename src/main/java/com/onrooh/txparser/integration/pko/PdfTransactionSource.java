package com.onrooh.txparser.integration.pko;

import com.onrooh.txparser.domain.Transaction;
import com.onrooh.txparser.domain.TransactionSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Slf4j
public class PdfTransactionSource implements TransactionSource {

    public static final String BANK_NAME = "PKO Bank Polski";

    @Override
    public List<Transaction> get(File file) {
        log.info("Get transactions from file: '{}'", file.getName());
        Document document = new Document(tryGetFileLines(file));
        return document.getTransactions();
    }

    private List<String> tryGetFileLines(File file) {
        try (PDDocument pdf = PDDocument.load(file)) {
            PDFTextStripper pdfStripper = new PDFTextStripper();
            String fullText = pdfStripper.getText(pdf);
            return List.of(fullText.split(System.lineSeparator()));
        } catch (IOException e) {
            log.error("Reading of file failed", e);
            throw new IllegalStateException(e);
        }
    }

}
