package com.onrooh.txparser.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@Builder
@EqualsAndHashCode
public class Account {
    private String number;
    private String currency;
    private String bank;
}
