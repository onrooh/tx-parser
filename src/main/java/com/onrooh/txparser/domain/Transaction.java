package com.onrooh.txparser.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@Builder
@EqualsAndHashCode
public class Transaction {
    private Integer id;
    private String account;
    private String currency;
    private LocalDate operationDate;
    private LocalDate executionDate;
    private String operationId;
    private String operationType;
    private Integer operationSum;
    private Integer balance;
    private String description;
}
