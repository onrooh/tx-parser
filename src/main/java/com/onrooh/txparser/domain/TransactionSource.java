package com.onrooh.txparser.domain;

import java.io.File;
import java.util.List;

public interface TransactionSource {

    /**
     * Load transactions from file or dir
     */
    List<Transaction> get(File file);

}
