package com.onrooh.txparser;

import com.onrooh.txparser.csv.CsvFormat;
import com.onrooh.txparser.domain.TransactionSource;
import com.onrooh.txparser.integration.pko.PdfTransactionSource;
import lombok.extern.slf4j.Slf4j;
import picocli.CommandLine;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

import static com.onrooh.txparser.Constants.VERSION;
import static com.onrooh.txparser.csv.CsvUtils.writeCSVFile;
import static com.onrooh.txparser.csv.TransactionCollector.toCSV;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@Slf4j
@CommandLine.Command(name = "tx-parser", mixinStandardHelpOptions = true, version = VERSION,
        description = "Parses file with transactions and put them into csv file")
public class App implements Callable<Integer> {

    private static final String PDF_EXTENSION = ".pdf";
    static final String OUTPUT_FILE = "transactions.csv";
    static final String DELIMITER = "|";
    static final String LINE_SEPARATOR = "\n";

    @CommandLine.Option(names = {"-i", "--input"}, required = true,
            description = "Path to pdf file or dir (no recursive processing) with transactions")
    private String inputPath;

    @CommandLine.Option(names = {"-o", "--output"}, defaultValue = OUTPUT_FILE,
            description = "Path for output file")
    private String outputPath;

    @CommandLine.Option(names = {"-d", "--delimiter"}, defaultValue = DELIMITER,
            description = "Delimiter between csv columns")
    private String delimiter;

    @CommandLine.Option(names = {"-l", "--line-separator"}, defaultValue = LINE_SEPARATOR,
            description = "Line separator")
    private String lineSeparator;

    private final TransactionSource transactionSource = new PdfTransactionSource();

    public void run(String inputPath, String outputPath, String delimiter, String lineSeparator) {
        String header = Stream.of(CsvFormat.values()).map(Enum::name).collect(joining(delimiter));
        List<String> transactions = loadTransactions(inputPath);

        writeCSVFile(header, transactions, outputPath, lineSeparator);
    }

    private List<String> loadTransactions(String pathStr) {
        File file = new File(pathStr);
        return file.isDirectory()
                ? loadTransactionsFromDirectory(file)
                : loadTransactionsFromFile(file);
    }

    private List<String> loadTransactionsFromDirectory(File file) {
        File[] files = file.listFiles((dir, name) -> name.endsWith(PDF_EXTENSION));
        return Arrays.stream(files == null ? new File[]{} : files)
                .flatMap(f -> loadTransactionsFromFile(f).stream())
                .collect(toList());
    }

    private List<String> loadTransactionsFromFile(File file) {
        try {
            return transactionSource.get(file).stream().collect(toCSV(DELIMITER));
        } catch (Exception e) {
            log.error("Cannot load transactions from file: '{}'", file.getName(), e);
            return List.of();
        }
    }

    @Override
    public Integer call() {
        new App().run(inputPath, outputPath, delimiter, lineSeparator);
        return 0;
    }

    public static void main(String[] args) {
        int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);
    }

}
