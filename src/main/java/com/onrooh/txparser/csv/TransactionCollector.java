package com.onrooh.txparser.csv;

import com.onrooh.txparser.domain.Transaction;
import lombok.RequiredArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

@RequiredArgsConstructor
public class TransactionCollector implements Collector<Transaction, List<String>, List<String>> {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final String separator;

    public static TransactionCollector toCSV(String separator) {
        return new TransactionCollector(separator);
    }

    @Override
    public Supplier<List<String>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<String>, Transaction> accumulator() {
        return (res, tx) ->
                res.add(String.join(separator,
                        tx.getAccount(),
                        tx.getCurrency(),
                        DATE_FORMAT.format(tx.getOperationDate()),
                        DATE_FORMAT.format(tx.getExecutionDate()),
                        tx.getOperationId(),
                        tx.getOperationType(),
                        String.valueOf(tx.getOperationSum()),
                        String.valueOf(tx.getBalance()),
                        emptyIfNull(tx.getDescription())
                ));
    }

    @Override
    public BinaryOperator<List<String>> combiner() {
        return (l1, l2) -> {
            l1.addAll(l2);
            return l1;
        };
    }

    @Override
    public Function<List<String>, List<String>> finisher() {
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.IDENTITY_FINISH);
    }

    private static String emptyIfNull(String input) {
        return input == null || input.isEmpty() ? "" : input;
    }

}
