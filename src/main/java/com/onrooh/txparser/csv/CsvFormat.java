package com.onrooh.txparser.csv;

public enum CsvFormat {
    ACCOUNT,
    CURRENCY,
    OPERATION_DATE,
    EXECUTION_DATE,
    OPERATION_ID,
    OPERATION_TYPE,
    OPERATION_SUM,
    BALANCE,
    DESCRIPTION;
}
