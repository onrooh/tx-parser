package com.onrooh.txparser.csv;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Slf4j
@UtilityClass
public class CsvUtils {

    public static void writeCSVFile(String header, List<String> transactions, String outputFile, String lineSeparator) {
        try (FileWriter writer = new FileWriter(outputFile)) {
            writer.write(header);
            writer.write(lineSeparator);

            for (String t : transactions) {
                writer.write(t);
                writer.write(lineSeparator);
            }
        } catch (IOException e) {
            log.error("Writing to file '{}' failed", outputFile);
        }
    }

}
