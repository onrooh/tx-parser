FROM maven:3.6.3-jdk-11-slim AS build
RUN mkdir /project
COPY . /project
WORKDIR /project
RUN mvn clean package -DskipTests


FROM adoptopenjdk/openjdk11:jre-11.0.9.1_1-alpine
RUN apk add dumb-init
ARG user_id
ARG group_id
RUN echo "User_id: $user_id; Group_id: $group_id"
RUN addgroup --system -g $group_id javauser
RUN adduser -u $user_id -g $group_id -S -s /bin/false javauser
RUN mkdir /app
COPY --from=build /project/target/pko-transaction-parser-*.jar /app/pko-transaction-parser.jar
WORKDIR /app
RUN chown -R javauser:javauser /app
USER javauser
ENTRYPOINT ["/usr/bin/dumb-init", "--", "java", "-jar", "pko-transaction-parser.jar"]